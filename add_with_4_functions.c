//Write a program to add two user input numbers using 4 functions.
#include <stdio.h>
int input();
int sum(int x, int y);
void output(int x, int y, int z);
void main()
{
int num1, num2, sum;
num1=input();
num2=input();
sum= num1+ num2;
output(num1,num2,sum);
}
int input()
{
int num;
printf("Enter number:\n");
scanf("%d",&num);
return num;
}
int sum(int x, int y)
{
return x+ y;
}
void output(int x, int y, int z)
{
printf("The sum of %d and %d is: %d\n",x, y, z);
}