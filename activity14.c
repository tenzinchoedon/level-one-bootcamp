#include<stdio.h>
#include<string.h>
typedef struct one
{
    char name[100];
    float wage;
    float hour;
    float gross;
} chik;
typedef struct two
{
    char name_1[100];
    int l;
} nyi;
void input_1(int t, chik *ptr)
{
    for(int i=0;i<t;i++)
    {
        scanf("%s %f",ptr->name,&ptr->wage);
        ptr++;
    }
}
void input_2(int c,nyi *ptr)
{
    for(int i=0;i<c;i++)
    {
        scanf("%s %d",ptr->name_1,&ptr->l);
        ptr++;
    }
}
void compute(int t, int c, chik *ptr1, nyi *ptr2)
{
    nyi *temp=ptr2;
    for(int i=0;i<t;i++)
    {
        ptr2=temp;
        ptr1->hour=0.0;
        ptr1->gross=0.0;
        for(int j=0;j<c;j++)
        {
            if(strcmp(ptr1->name,ptr2->name_1)==0)
            {
                ptr1->hour+=ptr2->l/60.0;
                if((ptr2->l/60.0)<=40.0)
                    ptr1->gross+=(ptr1->wage*ptr2->l/60.0);
                else
                    ptr1->gross+=(ptr1->wage*40.0+(ptr2->l/60.0)-40.0*1.5*ptr1->wage);
            }
            ptr2++;
        }
        ptr1++;
    }
}
void display(int t, chik *ptr)
{
    for(int i=0;i<t;i++)
    {
        if(ptr->gross>0)
            printf("%s: %.2f hours, $%.2f\n",ptr->name,ptr->hour,ptr->gross);
        ptr++;
    }
}
int main(void)
{
    int t;
    scanf("%d",&t);
    chik arr1[t];
    input_1(t,arr1);
    int c;
    scanf("%d",&c);
    nyi arr2[100];
    input_2(c,arr2);
    compute(t,c,arr1,arr2);
    display(t,arr1);
    return 0;
}