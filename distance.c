//WAP to find the distance between two point using 4 functions.
#include<stdio.h>
#include<math.h>
float input()
{
float temp;
scanf("%f",&temp);
return temp;
};

float calcdist(float a, float b, float c, float d)
{
return (sqrt((pow((b-a),2)+(pow((d-c),2)))));
};

void output(float x)
{
printf("The distance between the points are %.2f",x);};

int main(){
float x1, x2, y1, y2,dist;
printf("For the first point:\n");
printf("Enter the value of X:\n");
x1 = input();
printf("Enter the value of Y:\n");
y1 = input();
printf("For the second point:\n");
printf("Enter the value of X:\n");
x2 =input();
printf("Enter the value of Y:\n");
y2 =input();
dist = calcdist(x1, x2, y1, y2);
output(dist);
return 0;
} 