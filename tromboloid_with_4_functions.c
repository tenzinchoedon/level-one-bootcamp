//WAP to find the volume of a tromboloid using 4 functions.
#include<stdio.h>
float input()
{
float temp;
scanf("%f", &temp);
return temp;
};
float calcvol(float h, float d, float b)
{
return((h*d*b)+(d/b))/3;
};
void output(float volume)
{
printf("The volume of the tromboloid is: %.2f", volume);
};
int main()
{
float h,d,b,vol;
printf("Enter the three parameter of the tromboloid : \n");
h = input();
d = input();
b = input();
vol = calcvol(h, d, b);
output(vol);
return 0;
}
