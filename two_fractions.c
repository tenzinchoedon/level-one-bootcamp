//WAP to find the sum of two fractions.
#include<stdio.h>

typedef struct fraction
{
int nume ,deno;
} fraction;

fraction input()
{
fraction c;
printf("Enter the numerator:\n");
scanf("%d",&c.nume);
printf("Enter the denominator:\n");
scanf("%d",&c.deno);
return c;
}

int gcd(int a,int b)
{
if(a==0)
{
return b;
}
return gcd(b%a,a);
}

fraction calc(fraction x,fraction y)
{
fraction s;
int a,b;
a=(x.nume*y.deno)+(x.deno*y.nume);
b=x.deno*y.deno;
int gc=gcd(a,b);
s.nume=a/gc;
s.deno=b/gc;
return s;
}

void output(fraction q)
{
printf("The total is: %d/%d",q.nume,q.deno);
}

int main()
{
fraction y,w,u;
printf("Enter the 1st fraction\n");
y=input();
printf("Enter the 2nd fraction\n");
w=input();
u=calc(y,w);
output(u);
}