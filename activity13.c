#include <stdio.h>
#include <math.h>
typedef struct vertex
{
    float x;
    float y;
};
typedef struct vertice
{
    struct vertex xy[3];
};
struct vertice input()
{
    struct vertice data;
    printf("Enter the coordinates of the rectangle:\n");
    scanf("%f %f", &data.xy[0].x, &data.xy[0].y);
    scanf("%f %f", &data.xy[1].x, &data.xy[1].y);
    scanf("%f %f", &data.xy[2].x, &data.xy[2].y);
    return data;
}
float dis(struct vertex vert1,struct vertex vert2)
{
    return (sqrt(pow((vert1.x - vert2.x), 2) + pow((vert1.y - vert2.y), 2)));
}
float Calc(struct vertice data)
{
    float area;
    float dist1 = dis(data.xy[0], data.xy[1]);
    float dist2 = dis(data.xy[0], data.xy[2]);
    float dist12 = dis(data.xy[1], data.xy[2]);
    if(dist1>=dist2 && dist1>=dist12)
    {
        area=dist2*dist12;
    }
    else if(dist2>=dist1 && dist2>=dist12)
    {
        area=dist1*dist12;
    }
    else
    {
        area=dist1*dist2;
    }
    return area;
}
void output(struct vertice data, float area)
{
    printf("Area of rectangle is (%.1f,%.1f),(%.1f,%.1f),(%.1f,%.1f) is %.1f\n", data.xy[0].x, data.xy[0].y, data.xy[1].x, data.xy[1].y, data.xy[2].x, data.xy[2].y, area);
}
int main()
{
    int n;
    float area;
    scanf("%d", &n);
    struct vertice rectangles[n];
    for (int i = 0; i < n; i++)
        rectangles[i] = input();
    for (int i = 0; i < n; i++)
    {
        area = Calc(rectangles[i]);
        output(rectangles[i], area);
    }
    return 0;
}