//WAP to find the sum of n fractions.
#include<stdio.h>

typedef struct fractions
{
 int nume,deno;
 }Fractions;

int get()
{
 int temp;
 printf("Enter the no. of fractions: \n");
 scanf("%d",&temp);
 if(temp < 0)
 {
  printf("Please enter a number:\n");
  return get();
  }
 return temp;
};

void inputs(Fractions *c, int num)
{
 for(int i = 0;i < num;i ++)
 {
  printf("Enter the numerator:\n");
  scanf("%d",&c[i].nume);
  printf("Enter the denominator:\n");
  scanf("%d",&c[i].deno);
  }
};

void output(Fractions temp,Fractions *c,int num)
{
 printf("The sum of the fractions ");
 for(int i=0;i<num;i++)
 {
    if(i==num-1)
    {
        printf(" %d/%d = %d/%d",c[i].nume,c[i].deno,temp.nume,temp.deno);
    }
    else
    {
    printf("%d/%d +",c[i].nume,c[i].deno);
        
    }
  }
};

int gcd(int x,int y){
    if(x==0){
        return y;
    }
    return gcd(y%x,x);
};

Fractions calculate(int num, Fractions *c)
{
  Fractions final;
  int x,y=0,test=1;
  for(int i =0;i<num;i++)
  {
    test = test*c[i].deno;
      
  }
    final.deno =test;
    for(int i =0;i<num;i++)
    {
        x=c[i].nume;
        for(int j=0;j<num;j++)
        {
            if(i!=j)
                {
                x=x*c[j].deno;
                 }
        }
        y=y+x;
     }

    final.nume=y/ gcd(y,test);
    final.deno=test/ gcd(y,test);
    return final; 
};

int main()
{
  int num;
  Fractions final;
  num = get();
  Fractions  c[num];
  inputs(c,num);
  final = calculate(num,c);
  output(final,c,num);
  return 0;
    
}