#include<stdio.h>
struct egyptian
{
	int nume, deno;
};

int get()
{
	int temp;
	printf("Enter The No. Of egyptian fraction: \n");
	scanf("%d",&temp);
	if( temp < 0)
    {
        printf("Enter a valid positive number \n");
        return get();
    }
    return temp;
}

void input(struct egyptian *a, int no )
{
	for(int i=0;i<no;i++)
	{
		printf("\n Enter the egyptian number: \n");
		scanf("%d",&a[i].deno);
	}
}

void output(struct egyptian temp, struct egyptian *a, int no)
{
	printf("The egyptian fractions now converted as:  ");
	for(int i=0;i<no;i++)
	{
		if(i==no-1)
		{
			printf("%d/%d = %d/%d", 1, a[i].deno,temp.nume,temp.deno);
			printf("\n");
		}
		else
		{
			printf("%d/%d +",1,a[i].deno);
		}
	}
}

int gcd(int x,int y)
{
	if(x==0)
	{
		return y;
	}
	return gcd(y%x,x);
}


struct egyptian calculate(int no, struct egyptian *a)
{
	struct egyptian final;
	int x, y=0, dem=1;
	for(int i=0; i<no;i++)
	{
		dem =dem*a[i].deno;
	}
	final.deno=dem;

	for(int i=0;i<no;i++)
	{
		x=1;
		for(int j=0;j<no;j++)
		{
			if(i!=j)
			{
				x=x*a[j].deno;
			}
		}
		y=y+x;
	}
	final.nume=y/gcd(y,dem);
	final.deno=dem/gcd(y,dem);
	return final;
}

int main()
{
    int n,no;
    struct egyptian final;
    printf("\nEnter the number of times you want to convert\n");
    scanf("%d",&n);
    for(int i=0;i<n;i++)
    {
        no=get();
        struct egyptian a[no];
        input(a,no);
        final=calculate(no,a);
        output(final,a,no);
    }
    return 0;
}