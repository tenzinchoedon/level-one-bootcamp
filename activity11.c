typedef struct 
{
  char name[100];
  int no_of_scored_works;
  int scored_works[100];
  float  final_score;
  char grade;
  
}student;
typedef struct 
{
    char course[100];
    int no_of_weights;
    float weights[100];
    int no_of_students;  
    student students[100];

}gradebook;

student input_student(int no_of_scored_works)
{
  student s;
  printf("Enter the name of the student \n");
  scanf("%s",s.name);
  s.no_of_scored_works= no_of_scored_works;
 printf("Enter the scores \n");
 for(int i=0;i<s.no_of_scored_works;i++)
  {
   scanf("%d",&s.scored_works[i]);
  }
 return s;
 }

gradebook input_grade_book()
{
  gradebook g;
  printf("Name of the course \n");
 scanf("%s",g.course);
printf("Number of students \n");
scanf("%d",&g.no_of_students);

printf("Number of weights \n");
scanf("%d",&g.no_of_weights);

printf(" Enter weights \n");
for(int i=0;i<g.no_of_weights;i++)
{
scanf("%f",&g.weights[i]); 
}
for(int i=0;i<g.no_of_students;i++)
{
 g.students[i]= input_student(g.no_of_weights);
}
return g;
}

void input_ngradebook(int n,gradebook g[n])
{
  for(int i = 0;i<n;i++)
  {
   g[i]=input_grade_book();
  }
}

float sumof_weights(float weights[],int n_weight)
{
float  sum=0.0;
for(int i=0;i<n_weight;i++)
{
sum+=weights[i];
}
}

char score_to_grade(float score)
{
if(score<60)
return 'F';
if(score<70)
return 'D';
if(score<80)
return 'C';
if(score<90)
return 'B';
if(score<100)
return 'A';
}

 void computeone_student(student *a,gradebook *b)
 {
a->final_score=0;
for(int i=0;i<a->no_of_scored_works;i++)
{
 a->final_score+=a->scored_works[i]*b->weights[i];
}

float w = sumof_weights(b->weights,b->no_of_weights);
a->final_score /= w;
a->grade=score_to_grade(a->final_score);
 }

void compute_allstudent(gradebook *b)
{
   for(int i=0;i<b->no_of_students;i++)
   {
    computeone_student(&b->students[i], b);
   }
}

void compute_allgradebooks(int n,gradebook g[n])
{
  for(int i=0;i<n;i++)
   {
    compute_allstudent(&g[i]);
   }
  
}

void printone_student(student n1)
{

printf("%s\t%0.2f %c\n ",n1.name,n1.final_score,n1.grade);

}

void printn_student(gradebook g1)
{
 for(int i=0;i<g1.no_of_students;i++)
  {
    printone_student(g1.students[i]);
  }

}
void print(int n,gradebook g[])
{
 for(int i=0;i<n;i++)
 {
   printf("%s\n",g[i].course);
   printn_student(g[i]);
   printf("\n");
 }

}

int main()
{
   int n;
   printf("Enter the number of gradebook");
   scanf("%d",&n);
   gradebook g[n];
   input_ngradebook(n,g);
   compute_allgradebooks(n,g);
   print( n,g);
   return 0;

}
